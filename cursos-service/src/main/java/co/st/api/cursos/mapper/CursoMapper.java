package co.st.api.cursos.mapper;

import co.st.api.cursos.dto.CursoDTO;
import co.st.api.cursos.mapper.base.EntityMapper;
import co.st.api.cursos.model.Curso;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CursoMapper extends EntityMapper<CursoDTO, Curso> {
}
